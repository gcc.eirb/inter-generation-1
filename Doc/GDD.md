# Game Design Document

## Concept général

Impossibilité de se déplacer dans des zones dénuées de lumière (du fait d’un dispositif que le joueur aura sur lui en permanence). Mort en cas d’immersion totale dans une zone non éclairée. Le but du joueur sera d’explorer le centre, salle après salle, dans l’optique d’établir la raison de sa présence ici ainsi que trouver le moyen d’en sortir. Le tout en résolvant les énigmes qui s’opposeront sans cesse à sa progression. 
Au cœur de cette ambiance mystérieuse englobant l’endroit, la narration environnementale sera de mise pour donner au joueur diverses informations au sujet de sa présence en ce lieu, tout en conservant le flou quant à son but précis.

## Les lumières 

Vous l’aurez compris, les lumières jouent un rôle prépondérant dans Aperteirb, il s’agit donc de définir par quels moyens le joueur pourra interagir avec elles pour continuer sa progression. 
Actions principales : Il sera possible d’éteindre/allumer des lumières présentes, via divers mécanismes, ou de les déplacer. Déplacer des éléments du décor pour débloquer ou au contraire faire obstacle au faisceau d’une source de lumière. Enfin, possibilité de rediriger des faisceaux via des jeux de miroir par exemple.

**Le joueur peut-il détruire des lumières?** Pas dans la plupart des cas, mais il sera intéressant de le lui permette dans certaines situations – car il s’agit d’une action relativement contre-intuitive.

**Les objets inertes sont-ils soumis aux mêmes contraintes?** Pas pour la grande majorité d’entre eux, qui ne seront pas dotés du même système que le personnage incarné par le joueur. Cependant certains éléments spécifiques auront été conçus spécifiquement pour avoir les mêmes propriétés, rajoutant ainsi des contraintes supplémentaires au joueur dans sa progression. Certains d’entre eux pourront éventuellement être dotés d’un système d’activation/désactivation. 

## Les ombres

Si les ombres forment évidemment un obstacle, il sera tout à fait possible d’en user autrement, par exemple comme des plateformes sur lesquelles prendre appui. De la même manière, grâce à un déplacement adapté d’une source de lumière par exemple, elles pourraient même former des ascenseurs ou autre moyen de locomotion. L’idée est d’utiliser au maximum toutes les possibilités offertes par la mécanique principale. 

## Les déplacements 

Les options de déplacements du joueur seront relativement limitées : marcher et sauter. Le fait de forcer la marche aura pour fonction de refléter plusieurs choses, notamment liées à la nature même du lieu. L’environnement dans lequel se trouve le joueur lui est inconnu, et son aspect n’engage pas particulièrement à la confiance, incitant plus à avancer prudemment. Ensuite, si courir permet de se rendre plus rapidement d’un point A à un point B, ici il s’agit d’abord de comprendre comment relier ces points, la course n’est donc d’aucune utilité. Enfin, forcer le joueur à marcher l’incitera à plus prêter attention aux multiples détails qui l’entourent, entre autres ce qui concerne la narration environnementale qui jouera un rôle important. Pas de course, donc. Ne pouvant pas courir, il n’y aurait aucune raison que le joueur puisse escalader ou s’accrocher à des rebords, ces actions lui seront donc elles aussi proscrites.\
La vitesse de déplacement du personnage sera modérée, toujours dans l’optique de renforcer le côté tranquille et oppressant du lieu. Cependant elle devra être suffisamment élevée dans le même temps, pour permettre au joueur de réussir d’éventuelles épreuves de timing. 